import { expect } from "chai";
import * as P from "../src/index";

describe("Promise each tests", () => {
    context("Success tests", () => {
        it("should be a function", () => {
            expect(P.each).to.be.a("function");
            expect(P.each).to.be.instanceOf(Function);
        });

        it("should return all Capital Letters in series way", async () => {

            const res = await P.each("Geronimo", async (char) => {
                            await P.delay(P.random(1000, 500));
                            return char.toUpperCase(); // No effect...
                        });
            expect(res).to.deep.equal(["G","E","R","O","N","I","M","O"]);
        });

    });
    // context("Error tests", () => {
    //     it("Should throw expects arguments",async () => {
    //         let res;
    //         await P.each().then(()=>{
    //             console.log("err.message");
    //         }).catch((err:any)=>{
    //             res = err.message;
    //         });
    //         expect(res).to.be.equal("This function must get arguments");

    //     });
    // });
});