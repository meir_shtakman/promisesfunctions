


import { expect } from "chai";
import * as P from "../src/index";

describe("Promise reduce tests", () => {
    context("Success tests", () => {
        it("should be a function", () => {
            expect(P.reduce).to.be.a("function");
            expect(P.reduce).to.be.instanceOf(Function);
        });

        it("should return sum of all the numbers", async () => {

            const res = await P.reduce(
                [51, 64, 25, 12, 93],
                async (total, num) => {
                    await P.delay(P.random(1000, 100));
                    return total + num;
                },
                0
            );
            expect(res).to.be.equal(245);
        });

    });
    // context("Error tests", () => {
    //     it("Should throw expects arguments",async () => {
    //         let res;
    //         await P.reduce().then(()=>{
    //             console.log("err.message");
    //         }).catch((err:any)=>{
    //             res = err.message;
    //         });
    //         expect(res).to.be.equal("This function must get arguments");

    //     });
    // });
});






