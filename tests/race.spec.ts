


import { expect } from "chai";
import * as P from "../src/index";

describe("Promise race tests", () => {
    context("Success tests", () => {
        it("should be a function", () => {
            expect(P.race).to.be.a("function");
            expect(P.race).to.be.instanceOf(Function);
        });

        it("should return the first resolving item", async () => {

            const res =  await P.race([
                            P.echo("first",4000),
                            P.echo("second",400),
                            P.echo("third",3000)
                            ]);
            expect(res).to.be.equal("second");
        });

    });
    // context("Error tests", () => {
    //     it("Should throw expects arguments",async () => {
    //         let res;
    //         await P.race().then(()=>{
    //             console.log("err.message");
    //         }).catch((err:any)=>{
    //             res = err.message;
    //         });
    //         expect(res).to.be.equal("This function must get arguments");

    //     });
    // });
});






