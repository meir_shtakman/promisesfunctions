


import { expect } from "chai";
import * as P from "../src/index";

describe("Promise FilterParallel tests", () => {
    context("Success tests", () => {
        it("should be a function", () => {
            expect(P.filterSeries).to.be.a("function");
            expect(P.filterSeries).to.be.instanceOf(Function);
        });

        it("should return array with letters", async () => {

            const res = await P.filterSeries("G<4!e3ro0ni1mo", async char => {
                            await P.delay( P.random(1000,500) );
                            return /^[A-Za-z]+$/.test(char); // test for alphabetic characters
                        });
            expect(res).to.deep.equal(["G","e","r","o","n","i","m","o"]);
        });

    });
    // context("Error tests", () => {
    //     it("Should throw expects arguments",async () => {
    //         let res;
    //         await P.filterSeries().then(()=>{
    //             console.log("err.message");
    //         }).catch((err:any)=>{
    //             res = err.message;
    //         });
    //         expect(res).to.be.equal("This function must get arguments");

    //     });
    // });
});






