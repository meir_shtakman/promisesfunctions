import { expect } from "chai";
import * as P from "../src/index";

describe("Promise all tests", () => {
    context("Success tests", () => {
        it("should be a function", () => {
            expect(P.all).to.be.a("function");
            expect(P.all).to.be.instanceOf(Function);
        });

        it("should resolved all 3 promises in array", async () => {
            const promise1 = P.echo("1 first resolved value", 3000);
            const promise2 = P.echo("2 second resolved value", 3000);
            const promise3 = P.echo("3 third resolved value", 3000);

            // /**   P.all()  **/
            const res = await P.all([promise1, promise2, promise3]);
            expect(res).to.deep.equal(["1 first resolved value", "2 second resolved value", "3 third resolved value"]);
        });

    });
    // context("Error tests", () => {
    //     it("Should throw expects arguments",async () => {
    //         let res;
    //         await P.all().then(()=>{
    //             console.log("err.message");
    //         }).catch((err:any)=>{
    //             res = err.message;
    //         });
    //         expect(res).to.be.equal("This function must get arguments");

    //     });
    // });
});