


import { expect } from "chai";
import * as P from "../src/index";

describe("Promise some tests", () => {
    context("Success tests", () => {
        it("should be a function", () => {
            expect(P.some).to.be.a("function");
            expect(P.some).to.be.instanceOf(Function);
        });

        it("should return the 2 fastest resolving items", async () => {

            const res =  await P.some([
                                    P.echo("first",4000),
                                    P.echo("second",400),
                                    P.echo("third",3000),
                                    P.echo("forth",3000),
                                    P.echo("fifth",500),
                                ],2);
            expect(res).to.deep.equal(["second","fifth"]);
        });

    });
    // context("Error tests", () => {
    //     it("Should throw expects arguments",async () => {
    //         let res;
    //         await P.some().then(()=>{
    //             console.log("err.message");
    //         }).catch((err:any)=>{
    //             res = err.message;
    //         });
    //         expect(res).to.be.equal("This function must get arguments");

    //     });
    // });
});






