import { expect } from "chai";
import * as P from "../src/index";

describe("Promise mapSeries tests", () => {
    context("Success tests", () => {
        it("should be a function", () => {
            expect(P.mapSeries).to.be.a("function");
            expect(P.mapSeries).to.be.instanceOf(Function);
        });

        it("should return all Capital Letters in series way", async () => {

            const res = await P.mapSeries("Geronimo", async char => {
                await P.delay(P.random(1000, 500));
                return char.toUpperCase(); // Modify each item in the iterable
            });
            expect(res).to.deep.equal(["G", "E", "R", "O", "N", "I", "M", "O"]);
        });

    });
    // context("Error tests", () => {
    //     it("Should throw expects arguments",async () => {
    //         let res;
    //         await P.mapSeries().then(()=>{
    //             console.log("err.message");
    //         }).catch((err:any)=>{
    //             res = err.message;
    //         });
    //         expect(res).to.be.equal("This function must get arguments");

    //     });
    // });
});