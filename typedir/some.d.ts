export declare function some(iterator: Iterable<any> | Promise<Iterable<any>>, num: number): Promise<unknown>;
