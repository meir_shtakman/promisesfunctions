export declare function filterSeries(iterator: Iterable<any> | Promise<Iterable<any>>, cb: (a: any, b?: number, c?: number) => Promise<boolean>): Promise<any[]>;
