export declare function mapParallel(arr: Iterable<any> | Promise<Iterable<any>>, cb: (a: any, b: number, c: number) => any): Promise<Iterable<any>>;
