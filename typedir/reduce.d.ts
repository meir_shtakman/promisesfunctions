export declare function reduce(iterator: Iterable<any> | Promise<Iterable<any>>, cb: (a: any, b: any, c: number, d: number) => any, initialValue: any): Promise<any>;
