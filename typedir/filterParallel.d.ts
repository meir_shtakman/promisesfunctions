export declare function filterParallel(iterator: Iterable<any> | Promise<Iterable<any>>, cb: (a: any, b: number, c: number) => boolean): Promise<any[]>;
