export declare function mapSeries(arr: Iterable<any> | Promise<Iterable<any>>, cb: (a: any, b: number, c: number) => any): Promise<any[]>;
