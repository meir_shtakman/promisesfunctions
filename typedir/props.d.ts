interface promiseObj {
    [index: string]: any;
}
export declare function props(promiseObj: promiseObj): Promise<promiseObj>;
export {};
