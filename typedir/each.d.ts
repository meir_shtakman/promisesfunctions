export declare function each(iterator: Iterable<any> | Promise<Iterable<any>>, cb: (a: any, b: number, c: number) => any): Promise<any[]>;
