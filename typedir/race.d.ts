export declare function race(iterator: Iterable<any> | Promise<Iterable<any>>): Promise<unknown>;
