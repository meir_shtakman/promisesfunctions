export async function some(
    iterator: Iterable<any> | Promise<Iterable<any>>,
    num: number
) {
    if (arguments.length === 0) {
        throw new Error("This function must get arguments");
    }
    return new Promise((res, rej) => {
        let count = 0;
        const arr: any[] = [];
        (iterator as Array<any>).forEach((p) => {
            p.then((result: any) => {
                if (count < num - 1) {
                    arr.push(result);
                    count++;
                } else {
                    arr.push(result);
                    res(arr);
                }
            });
        });
    });
}
