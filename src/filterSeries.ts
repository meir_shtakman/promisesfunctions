import { mapSeries } from "./mapSeries.js";

export async function filterSeries(
    iterator: Iterable<any> | Promise<Iterable<any>>,
    cb: (a: any, b?: number, c?: number) => Promise<boolean>
) {
    if (arguments.length === 0) {
        throw new Error("This function must get arguments");
    }
    const arr: Iterable<boolean> = await mapSeries(iterator, cb);
    iterator = await iterator;
    return Array.from(iterator).filter(
        (value, i) => (arr as Array<boolean>)[i]
    );
}
