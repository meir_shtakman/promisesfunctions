export async function each(
    iterator: Iterable<any> | Promise<Iterable<any>>,
    cb: (a: any, b: number, c: number) => any
) {
    if (arguments.length === 0) {
        throw new Error("This function must get arguments");
    }
    let count = 0;
    const arr = [];
    iterator = await iterator;
    for (const value of iterator) {
        arr.push(await cb(value, count, Array.from(iterator).length));
        console.log(value, count);
        count++;
    }

    return Array.from(arr);
}
