import { all } from "./all.js";
export async function mapParallel(
    arr: Iterable<any> | Promise<Iterable<any>>,
    cb: (a: any, b: number, c: number) => any
) {
    if (arguments.length === 0) {
        throw new Error("This function must get arguments");
    }
    const newArr: any[] = [];
    arr = Array.from(await arr);
    Array.from(arr).forEach(async (elemnt, i) => {
        console.log("start func tion");
        const prom = cb(elemnt, i, (arr as Array<any>).length);
        newArr.push(prom);
    });
    console.log(newArr);
    const arrDone = await all(newArr);
    console.log(arrDone);
    return arrDone;
}
