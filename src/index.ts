import log from "@ajar/marker";
export { all } from "./all.js";
export { each } from "./each.js";
export { filterParallel } from "./filterParallel.js";
export { filterSeries } from "./filterSeries.js";
export { mapParallel } from "./mapParallel.js";
export { mapSeries } from "./mapSeries.js";
export { reduce } from "./reduce.js";
export { race } from "./race.js";
export { some } from "./some.js";
export { props } from "./props.js";

export const delay = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));

//--------------------------------------------------

// export const echo = async (msg, ms) => {
//   await delay(ms);
//   return msg;
// };

//--------------------------------------------------

export const echo = async (msg: string, ms: number) => {
    log.yellow(`--> start ${msg}`);
    await delay(ms);
    log.blue(`finish <-- ${msg}`);
    return msg;
};

//--------------------------------------------------

export const random = (max: number, min = 0) =>
    min + Math.round(Math.random() * (max - min));
