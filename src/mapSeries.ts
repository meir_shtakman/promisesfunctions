export async function mapSeries(
    arr: Iterable<any> | Promise<Iterable<any>>,
    cb: (a: any, b: number, c: number) => any
) {
    if (arguments.length === 0) {
        throw new Error("This function must get arguments");
    }
    let count = 0;
    let returnValue: any;
    const returnArr: any[] = [];
    arr = await arr;
    for (const value of arr) {
        await value;
        returnValue = await cb(value, count, (arr as Array<any>).length);
        returnArr.push(returnValue);
        console.log(value, count);
        count++;
    }

    return returnArr;
}
