export async function all(
    promiseArr: Iterable<any> | Promise<Iterable<any>>
): Promise<Iterable<any>> {
    if (arguments.length === 0) {
        throw new Error("This function must get arguments");
    }
    promiseArr = await promiseArr;
    const size: number = Array.from(promiseArr).length;
    promiseArr = await promiseArr;
    const resolveArr: any[] = [];
    for (let i = 0; i < size; i++) {
        const resolve = await Array.from(promiseArr)[i];
        resolveArr.push(resolve);
        console.log(i, "async", resolve);
    }

    return resolveArr;
}
