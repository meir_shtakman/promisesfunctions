export async function reduce(
    iterator: Iterable<any> | Promise<Iterable<any>>,
    cb: (a: any, b: any, c: number, d: number) => any,
    initialValue: any
) {
    if (arguments.length === 0) {
        throw new Error("This function must get arguments");
    }
    const pending: any = (iterator as Array<any>).reduce(
        async (acc: any, val: any, count: number) => {
            val = await val;
            return await cb(
                await acc,
                val,
                count,
                (iterator as Array<any>).length
            );
        },
        initialValue
    );
    const returnValue: any = await pending;
    return returnValue;
}
