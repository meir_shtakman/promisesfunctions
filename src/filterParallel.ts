import { all } from "./all.js";
import { mapParallel } from "./mapParallel.js";

export async function filterParallel(
    iterator: Iterable<any> | Promise<Iterable<any>>,
    cb: (a: any, b: number, c: number) => Promise<boolean>
) {
    if (arguments.length === 0) {
        throw new Error("This function must get arguments");
    }
    let arr: Iterable<boolean> = await mapParallel(iterator, cb);
    arr = await all(arr);
    iterator = await iterator;
    return Array.from(iterator).filter(
        (value, i) => (arr as Array<boolean>)[i]
    );
}
