interface promiseObj {
    [index: string]: any;
}

export async function props(promiseObj: promiseObj): Promise<promiseObj> {
    if (arguments.length === 0) {
        throw new Error("This function must get arguments");
    }
    const resolveObj: promiseObj = {};
    for (const [key, value] of Object.entries(promiseObj)) {
        const resolve = await value;
        resolveObj[key] = resolve;
        console.log(key, "async", resolve);
    }

    return resolveObj;
}
