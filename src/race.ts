export async function race(iterator: Iterable<any> | Promise<Iterable<any>>) {
    if (arguments.length === 0) {
        throw new Error("This function must get arguments");
    }
    return new Promise((res, rej) => {
        (iterator as Array<any>).forEach((p) => p.then(res).catch(rej));
    });
}
